#!/bin/bash

sleep .3s

multilib(){ 
    # this option will avoid any problem with packages install
  [[ sed -i "${_multilib}s/^#//" /etc/pacman.conf ]]
  if [[ -z $_multilib ]]; then
        echo -e "\n[multilib]\nInclude = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf
        echo -e '\nMultilib repository added into pacman.conf file'
      else
  exit 0
  } 
  
  add_server() {
    echo [archlinuxfr] \
    SigLevel=Never    \
    Server=http://repo.archlinux.fr/$arch > /etc/pacman.conf << EOF
}

sleep .3s

sed -i '/%wheel ALL=(ALL) NOPASSWD: ALL/s/^#//' /etc/sudoers

sleep .3s

