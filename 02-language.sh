#!/bin/bash

####02...sh

pacstrap /mnt base base-devel grub os-prober bash-completion dialog networkmanager network-manager-applet linux-lts linux-lts-headers xf86-input-synaptics intel-ucode xorg tmux linux-firmware dhcpcd dhclient vim 

genfstab -U /mnt >> /mnt/etc/fstab

mkdir -p /mnt/scripts
chmod 777 /mnt/scripts
mv *.sh /mnt/scripts &>/dev/null
cd /mnt/scripts &>/dev/null
arch-chroot /mnt /scripts/chroot.sh
./03-settings.sh


