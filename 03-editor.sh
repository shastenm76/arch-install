#!/bin/bash

sleep .3s

cd /mnt

sleep .3s

read hostname | echo -e "Como se llama tu computadora?"
echo $hostname > /etc/hostname
sleep .3s
passwd

read name | echo -e "Como se llama usted?"
useradd -m -G audio,disk,lp,optical,storage,video,wheel,games,power,scanner -s /bin/bash $name
sleep .3s
passwd $name

ln -sf /usr/share/zoneinfo/$TZuser /etc/localtime

hwclock --systohc

echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "en_US ISO-8859-1" >> /etc/locale.gen

locale-gen

./04-servers.sh